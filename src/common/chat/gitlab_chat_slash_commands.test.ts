import { defaultSlashCommands } from './gitlab_chat_slash_commands';

describe('GitLab Chat Slash Commands', () => {
  it('returns pre-defined set of slash commands', () => {
    expect(defaultSlashCommands).toEqual([
      expect.objectContaining({ name: '/reset' }),
      expect.objectContaining({ name: '/clean' }),
      expect.objectContaining({ name: '/tests' }),
      expect.objectContaining({ name: '/refactor' }),
      expect.objectContaining({ name: '/explain' }),
    ]);
  });
});
