include:
  # remove the `.latest` from the following templates after 16.0 GitLab release
  # the `.latest` indicates the "nightly" version of the job definition
  # when we remove the `.latest`, we'll be using the stable job definition
  # https://gitlab.com/gitlab-org/gitlab/-/issues/217668#note_1136574790
  - template: Jobs/SAST.latest.gitlab-ci.yml
  - template: Jobs/Dependency-Scanning.latest.gitlab-ci.yml
  - template: Jobs/Secret-Detection.latest.gitlab-ci.yml
  - template: Jobs/License-Scanning.latest.gitlab-ci.yml
  - project: 'gitlab-org/quality/pipeline-common'
    file: '/ci/danger-review.yml' # danger-review job below

# why: Similar to https://gitlab.com/gitlab-org/cli/-/merge_requests/1352 to avoid
# upstream breaking changes
danger-review:
  variables:
    GITLAB_DANGERFILES_VERSION: "4.1.0"

variables:
  SAST_EXCLUDED_ANALYZERS: "eslint"

# run the pipeline only on MRs, tags, and default branch
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

default:
  tags:
    - gitlab-org

image: node:18-slim

stages:
  - test
  - package
  - publish

check_docs_markdown:
  stage: test
  needs: []
  image: registry.gitlab.com/gitlab-org/gitlab-docs/lint-markdown:alpine-3.19-vale-3.0.7-markdownlint-0.39.0-markdownlint2-0.12.1
  script:
    # Lint prose
    - vale --minAlertLevel error docs README.md CONTRIBUTING.md
    # Lint Markdown
    - markdownlint-cli2 'docs/**/*.md' README.md CONTRIBUTING.md

lint:
  stage: test
  script:
    - apt-get update && apt-get install -y git
    - npm ci
    - cd webviews/vue && npm ci && cd ../.. # webview dependencies
    - cd webviews/vue2 && npm ci && cd ../.. # webview dependencies
    - git checkout . # the npm install automatically fixes package.json formatting issues, but we need the CI to discover them so we revert any changes
    - npm run lint

lint_commit:
  stage: test
  script:
    - apt-get update && apt-get install -y git
    - git fetch origin $CI_MERGE_REQUEST_TARGET_BRANCH_NAME $CI_COMMIT_SHA
    - cd scripts/commit-lint && npm ci
    - ./lint.sh
  rules:
    - if: '$CI_MERGE_REQUEST_IID'
      when: always

check-ci-variables:
  stage: test
  script:
    - npm ci
    - npm run update-ci-variables
  allow_failure: true # could be caused by changes in gitlab-org/gitlab repo, not related to current branch
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: always
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - 'src/ci/ci_variables.json'
        - 'scripts/update_ci_variables.js'

check-icons:
  stage: test
  script:
    - apt-get update && apt-get install -y git wget
    - ./scripts/check_icons_are_up_to_date.sh
  allow_failure: true # can be caused by changes in https://gitlab.com/gitlab-org/editor-extensions/gitlab-ide-icons
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: always
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - 'src/assets/icons/*'

test-unit:
  stage: test
  script:
    - apt-get update && apt-get install -y git
    - npm ci
    - npm run test:unit -- --coverage
  artifacts:
    when: always
    reports:
      junit:
        - reports/unit.xml

test-integration:
  stage: test
  variables:
    DISPLAY: ':99.0'
  script:
    - apt-get update
    - apt-get install -y xvfb libxtst6 libnss3 libgtk-3-0 libxss1 libasound2 libsecret-1-0 libgbm-dev git
    - npm ci
    - echo $DISPLAY
    - /usr/bin/Xvfb :99 -screen 0 1024x768x24 > /dev/null 2>&1 &
    - npm run test:integration

test-webview:
  stage: test
  script:
    - cd webviews/vue
    - npm ci
    - npm run test

test-webview2:
  stage: test
  script:
    - cd webviews/vue2
    - npm ci
    - npm run test -- --coverage
  artifacts:
    when: always
    reports:
      junit:
        - reports/unit.xml

test-browser-build:
  stage: test
  script:
    - npm ci
    - npm run build:browser

test-e2e:
  stage: test
  script:
    - apt-get update
    - apt-get install -y xvfb libxtst6 libnss3 libgtk-3-0 libxss1 libasound2 libsecret-1-0 libgbm-dev git sudo default-jre
    - npm ci
    - npm run package
    - cd test/e2e
    - npm install
    - sudo -u node TEST_GITLAB_TOKEN=$TEST_GITLAB_TOKEN xvfb-run -a npm run test:e2e
  allow_failure: true
  artifacts:
    expire_in: 10 days
    when: always
    paths:
      - '**/**/allure-report/*.html'
  rules:
    - if: '$TEST_GITLAB_TOKEN'
      when: manual

.package:
  stage: package
  script:
    - npm ci
    - npm run package
  artifacts:
    paths:
      - 'dist-desktop/*.vsix'

package_release:
  extends: .package
  artifacts:
    expire_in: 1 year
  only:
    - tags

# We test that packaging works to prevent failed releases
# Without this task we would only find out packaging errors after tagging a release
package_test:
  extends: .package
  artifacts:
    expire_in: 10 days
  except:
    - tags

publish_marketplace:
  stage: publish
  script:
    - npx vsce publish --packagePath dist-desktop/*.vsix -p $AZURE_ACCESS_TOKEN
  when: manual
  only:
    - tags

publish_open_vsx:
  stage: publish
  script:
    - npx ovsx publish dist-desktop/*.vsix -p $OPENVSX_ACCESS_TOKEN
  when: manual
  only:
    - tags
